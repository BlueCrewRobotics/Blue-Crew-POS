#
# Blue Crew Point of Sale Register
# (c) 2018 Blue Crew Robotics
# Author: Matt Gallant
#

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from openpyxl import *
from functools import partial
from socket import socket, AF_INET, SOCK_STREAM
from pprint import pprint

import datetime
import sys
import os
import os.path
import ctypes
import time
import platform
import json
import math

import libraries.pydb as db
import libraries.common as common

# Main GUI Thread
class POS(QWidget):
    
    def __init__(self):
        super().__init__()

        # Windows Taskbar Icon Fix
        if (platform.system() == "Windows"):
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('org.bluecrew6153.register')

        # Initialize Variables
        self.order = []
        self.itemNumber = 0
        self.isServerSet = False
        self.drawerInitialized = False
        self.drawerInitAmount = 0

        # Start GUI & Terminal Client
        self.initUI()
        self.initClient()

    # Detect Keypresses
    def keyPressEvent(self, event):

        # Query Item
        if event.key() == Qt.Key_Q:
            self.addItemToOrder()
        
        # Finalize Transaction
        elif event.key() == Qt.Key_F:
            self.completeTransaction()
        
        # Cancel Order
        elif event.key() == Qt.Key_C:
            self.cancelOrder()

        # Continue
        event.accept()

    # Close App Signal
    def closeEvent(self, event):

        # Exit App
        self.exitApp()
        event.ignore()
        
    # GUI Creation
    def initUI(self):

        # Set App Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        # Create Add Item Button
        self.addBtn = QPushButton('Add Item to Order (Q)', self, default = False, autoDefault = False)
        self.addBtn.setIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'add.png'))
        self.addBtn.clicked.connect(self.addItemToOrder)

        # Create Logo
        self.logo = QLabel(self)
        logoImage = QPixmap(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png').scaled(64, 64)
        self.logo.setPixmap(logoImage)

        # Create Title
        self.titleLabel = QLabel(self)
        self.titleLabel.setText("Blue Crew POS Register")
        self.titleLabel.setFont(QFont("Helvetica", 20, QFont.Bold))

        # Create Order Button
        self.orderBtn = QPushButton('Complete Order (F)', self)
        self.orderBtn.setIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'cart.png'))
        self.orderBtn.clicked.connect(self.completeTransaction)

        # Create Cancel Button
        self.cancelButton = QPushButton('Cancel Order (C)', self)
        self.cancelButton.setIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'clear.png'))
        self.cancelButton.clicked.connect(self.cancelOrder)

        # Create Help Button
        self.helpButton = QPushButton('Help', self)
        self.helpButton.clicked.connect(self.help)

        # Create Settings Button
        self.settingsButton = QPushButton('Settings', self)
        self.settingsButton.clicked.connect(self.openSettings)

        # Create Misc Button
        self.miscButton = QPushButton('Misc.', self)
        self.miscButton.clicked.connect(self.miscOpts)

        # Check if Fullscreen is Enabled
        self.fullscreenEnabled = db.get("fullscreen")

        # Set Window Options
        self.setGeometry(0, 0, 900, 600)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Register')

        if (self.fullscreenEnabled == "True" or self.fullscreenEnabled == None):
            self.showFullScreen()

        # Set Window Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        # Create Main Order Table
        self.createTable()

        # Add Widgets to Top Layout
        self.top_hbox = QHBoxLayout()
        self.top_hbox.addWidget(self.logo)
        self.top_hbox.addSpacing(10)
        self.top_hbox.addWidget(self.titleLabel)
        self.top_hbox.addStretch()
        self.top_hbox.addWidget(self.addBtn)
        self.top_hbox.addWidget(self.cancelButton)
        self.top_hbox.addWidget(self.orderBtn)

        # Add Widgets to Bottom Layout
        self.bottom_hbox = QHBoxLayout()
        self.bottom_hbox.addWidget(self.helpButton)
        self.bottom_hbox.addStretch()
        self.bottom_hbox.addWidget(self.settingsButton)
        self.bottom_hbox.addStretch()
        self.bottom_hbox.addWidget(self.miscButton)
 
        # Add Widgets to Layout
        self.layout = QVBoxLayout()
        self.layout.addLayout(self.top_hbox)
        self.layout.addSpacing(10)
        self.layout.addWidget(self.tableWidget)
        self.layout.addLayout(self.bottom_hbox)
        self.setLayout(self.layout)
 
        # Show GUI
        self.show()

    # Show Help Window
    def help(self):
        self.helpWindow = HelpPanel()
        self.helpWindow.show()

    # Encrypt and Set Admin Code
    def setupAdminCode(self, code):
        db.set("code", common.encode("bc6153", code))

    # Initialize Terminal Client
    def initClient(self):        
        
        # Check if Server Address has Been Set
        if (db.get("serverAddress") != None):
            if (len(db.get("serverAddress")) > 0):

                # Set Host and Port of Server
                self.host = db.get("serverAddress")
                self.port = 8888

                self.serverSocket = socket(AF_INET, SOCK_STREAM)

                try:
                    self.serverSocket.connect((self.host, self.port))
                    self.isServerSet = True
                except:
                    QMessageBox.critical(self, 'Server Error!', "Blue Crew POS Register could not connect to the Blue Crew POS Server using IP Address: " + db.get("serverAddress") + ". You can continue to use Blue Crew POS Register without connecting to the server though. To try reconnecting, please restart Blue Crew POS Register.", QMessageBox.Ok)

    # Generate Order Table 
    def createTable(self):
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(5)
        self.tableWidget.move(0,0)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Size"))
        self.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Price"))
        self.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem("Edit"))
        self.tableWidget.setHorizontalHeaderItem(4, QTableWidgetItem("Delete"))

    # Cancel Order
    def cancelOrder(self):

        cancelOrder = QMessageBox.question(self, 'Cancel Order?', "Are you sure that you would like to cancel the order?", QMessageBox.Yes | QMessageBox.No)

        if (cancelOrder == QMessageBox.Yes):
            self.clearOrder()

    # Clear Order
    def clearOrder(self):
        self.order = []
        self.itemNumber = 0
        self.tableWidget.setRowCount(0)

    # Add and Item to the Order Table
    def addToTable(self, item):

        code, name, size, price = self.getItems(item)

        # If Item Exists
        if (code != False):

            # Set Cost Equal to the Price Rounded to Two Decimals
            cost = "$" + str(format(price, ".2f"))

            # Get Row Count and Insert a Row After
            rowPosition = self.tableWidget.rowCount()
            self.tableWidget.insertRow(rowPosition)

            # Set Row Elements
            self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(name))
            self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(size))
            self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(cost))

            # Create Edit Button
            editBtn = QPushButton(self.tableWidget)
            editBtn.setText('Edit')
            editBtn.clicked.connect(partial(self.editItem, self.itemNumber))
            self.tableWidget.setCellWidget(rowPosition, 3, editBtn)

            # Create Delete Button
            deleteBtn = QPushButton(self.tableWidget)
            deleteBtn.setText('Delete')
            deleteBtn.clicked.connect(partial(self.deleteItem, self.itemNumber))
            self.tableWidget.setCellWidget(rowPosition, 4, deleteBtn)

            # Increment Item Number
            self.itemNumber += 1

    # Delete Item
    def deleteItem(self, item):

        # Delete Item
        self.order.pop(item)
        self.itemNumber = 0

        # Delete Table Rows
        self.tableWidget.setRowCount(0)

        # Reload Table Rows
        for name in self.order:
            self.addToTable(name)

    # Complete Transaction / Save to Spreadsheet / Send to Terminal
    def completeTransaction(self):

        if (self.drawerInitialized == True):

            totalCost = 0
            
            for text in self.order:
                
                code, name, size, price = self.getItems(text)

                # Check if Item Exists
                if (code != False):

                    # Add to Total Cost
                    totalCost += price

            orderName, ok = QInputDialog.getText(self, 'Order Name', 'Name on the Order (Leave Blank if Customer Line is Short):')
            if ok:

                acceptTotalMessageBox = QMessageBox()
                acceptTotalMessageBox.setIcon(QMessageBox.Question)
                acceptTotalMessageBox.setWindowTitle('Complete Order')
                acceptTotalMessageBox.setText('Total Due: $' + str(format(totalCost, '.2f')) + '. Select Payment Type.')
                acceptTotalMessageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
                acceptTotalMessageBox.setDefaultButton(QMessageBox.Yes)
                cashButton = acceptTotalMessageBox.button(QMessageBox.Yes)
                cashButton.setText('Cash')
                cardButton = acceptTotalMessageBox.button(QMessageBox.No)
                cardButton.setText('Card')
                acceptTotalMessageBox.exec_()

                if acceptTotalMessageBox.clickedButton() == cashButton:
                    paymentType = "Cash"

                    cashIn, ok = QInputDialog.getText(self, 'Cash In', 'Please enter the amount of cash given to you by the customer:')

                    if ok:                    
                        try:
                            changeDue = float(cashIn) - float(totalCost)

                            if (changeDue < 0):
                                changeDueMessageBox = QMessageBox()
                                changeDueMessageBox.setIcon(QMessageBox.Information)
                                changeDueMessageBox.setWindowTitle('Change Due')
                                changeDueMessageBox.setText('$' + str(format(float(cashIn), '.2f')) + " is not enough to pay for the $" + str(format(float(totalCost), '.2f')) + " order.")
                                changeDueMessageBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                                changeDueMessageBox.setDefaultButton(QMessageBox.Ok)
                                changeDueMessageBox.exec_()
                                return
                            else:
                                changeDueMessageBox = QMessageBox()
                                changeDueMessageBox.setIcon(QMessageBox.Information)
                                changeDueMessageBox.setWindowTitle('Change Due')
                                changeDueMessageBox.setText('Change Due: $' + str(format(changeDue, '.2f')) + ".")
                                changeDueMessageBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                                changeDueMessageBox.setDefaultButton(QMessageBox.Ok)
                                changeDueMessageBox.exec_()

                            if changeDueMessageBox.clickedButton() == changeDueMessageBox.button(QMessageBox.Cancel):
                                return

                        except:
                            changeDueMessageBox = QMessageBox()
                            changeDueMessageBox.setIcon(QMessageBox.Critical)
                            changeDueMessageBox.setWindowTitle('Not a Number')
                            changeDueMessageBox.setText("Please enter a number, not '" + str(cashIn) + "'.")
                            changeDueMessageBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                            changeDueMessageBox.setDefaultButton(QMessageBox.Ok)
                            changeDueMessageBox.exec_()
                            return
                    else:
                        return

                elif acceptTotalMessageBox.clickedButton() == cardButton:
                    paymentType = "Card"

                    cardMessageBox = QMessageBox()
                    cardMessageBox.setIcon(QMessageBox.Information)
                    cardMessageBox.setWindowTitle('Swipe Card')
                    cardMessageBox.setText('Please use the Square to pay $' + str(format(totalCost, '.2f')) + ".")
                    cardMessageBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                    cardMessageBox.setDefaultButton(QMessageBox.Ok)
                    cardMessageBox.exec_()

                # Check if Spreadsheet Exists
                if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
                    
                    # Openpyxl Initialization
                    workbook_name = db.get("sheet")
                    wb = load_workbook(workbook_name)
                    page = wb.active

                    # Create Array of Data to Send to Terminal
                    dataToSend = []

                    # For Each Item
                    for text in self.order:

                        code, name, size, price = self.getItems(text)

                        if (code != False and code != "test"):

                            # Append to Spreadsheet
                            page.append([name, size, price, paymentType, datetime.datetime.now(), orderName])

                            # Append to Data to Send Array
                            dataToSend.append(code)
                        
                        elif (code != False and code == "test"):
                            
                            # Append to Data to Send Array
                            dataToSend.append(code)
                                            
                    # Check if Terminal is Set
                    if (self.isServerSet == True):

                        # Turn Data to Send Array into a Comma Separated String
                        data = ", ".join(dataToSend)
                        data = "r/" + orderName + "|" + data

                        # Send Data String to Terminal
                        self.serverSocket.sendall(data.encode("utf8"))

                        # Sleep to Prevent Lost Packets
                        time.sleep(0.2)

                    # Save Spreadsheet
                    wb.save(db.get("sheet"))

                    # Clear Order to Start Over Again
                    self.clearOrder()
                else:
                    
                    # Spreadsheet Doesn't Exist
                    QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu!", QMessageBox.Ok)
        else:
            QMessageBox.critical(self, 'Drawer Not Initialized', "Please initialize your drawer by going to <b>Misc. -> Drawer Functions -> Initialize Drawer</b>.", QMessageBox.Ok)

    # Add Item to Order
    def addItemToOrder(self):
            query = self.itemSelect()
            
            if (query != None):
                self.addToTable(query)
                self.order.append(query)

    def getItems(self, item):

        # Send Data String to Terminal
        data = "q/" + item
        self.serverSocket.sendall(data.encode("utf8"))

        data = self.serverSocket.recv(5120).decode("utf8")
        print("HEY: " + data)

        if (data[0:1] == "s"):
            print("DATA: " + data)
            code, item, size, cost = data[2:].split("::")
            return code, item, size, float(cost)
        if (data[0:1] == "r"):
            newData = self.serverSocket.recv(5120).decode("utf8")
            print("new data: " + data)
            
            try:
                code, item, size, cost = newData[2:].split("::")
                return code, item, size, float(cost)
            except:
                return False, False, False, False
        else:
            return False, False, False, False

    def itemSelect(self):

        text, ok = QInputDialog.getText(self, 'Item Query', 'Enter Item Code:')
        
        if ok:
            code, name, size, price = self.getItems(text)

            print("Code: " + str(code))

            if (code != False):
                return code
            else:
                QMessageBox.information(self, 'Query Error!', "The item: '" + text + "' does not exist!", QMessageBox.Ok)
                return None
        else:
            return None

    def editItem(self, item):

        # Get Item Code
        query = self.itemSelect()

        # If Item Exists
        if (query != None):

            # Add to Order Array
            self.order[item] = query

        # Clear Table
        self.itemNumber = 0
        self.tableWidget.setRowCount(0)

        # Reload Table
        for name in self.order:
            self.addToTable(name)

    # Open Settings Window
    def openSettings(self):

        # Get Admin Code
        text, ok = QInputDialog.getText(self, 'Admin Code', 'Enter Admin Code:', QLineEdit.Password)

        if ok:
            if (common.encode("bc6153", text) == db.get("code")):

                # Open Settings Window
                self.settingsWindow = SettingsPanel()
                self.settingsWindow.show()
            else:

                # Invalid Code
                QMessageBox.warning(self, 'Invalid Code!', "The admin code entered is not valid!", QMessageBox.Ok)

    # Misc. Options
    def miscOpts(self):

            miscMessageBox = QMessageBox()
            miscMessageBox.setIcon(QMessageBox.Question)
            miscMessageBox.setWindowTitle('Misc. Options')
            miscMessageBox.setText('Please select an action.')
            miscMessageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.Help | QMessageBox.Ok | QMessageBox.Cancel)
            miscMessageBox.setDefaultButton(QMessageBox.Cancel)

            drawerFunctionsButton = miscMessageBox.button(QMessageBox.Yes)
            drawerFunctionsButton.setText('Drawer Functions')

            salesReportButton = miscMessageBox.button(QMessageBox.Help)
            salesReportButton.setText('Sales Report')

            quitButton = miscMessageBox.button(QMessageBox.Ok)
            quitButton.setText('Exit')
        
            miscMessageBox.exec_()

            if (miscMessageBox.clickedButton() == drawerFunctionsButton):
                self.drawerFunctions()
            elif (miscMessageBox.clickedButton() == salesReportButton):
                self.salesReport()
            elif (miscMessageBox.clickedButton() == quitButton):
                self.exitApp()

    # Generate Sales Report
    def salesReport(self):
        # Data: Gross Sales, Drawer Init Value, Drawer Total Value

        # Check if Spreadsheet Exists
        if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
            
            # Openpyxl Initialization
            workbook_name = db.get("sheet")
            wb = load_workbook(workbook_name)
            page = wb.active

            grossSales = 0
            drawerTotal = 0

            for row in page.iter_rows(min_row=2):
                name = row[0].value
                price = row[2].value
                
                if (name == "Drawer Initialization" or name == "Drawer Deinitialization" or name == "Drawer Addition" or name == "Drawer Deduction"):
                    drawerTotal += price
                else:
                    grossSales += float(price)
                    drawerTotal += price

            salesReportMessageBox = QMessageBox()
            salesReportMessageBox.setIcon(QMessageBox.Information)
            salesReportMessageBox.setWindowTitle('Sales Report')
            salesReportMessageBox.setText('Gross Sales: $' + str(format(grossSales, ".2f")) + '\nDrawer Total: $' + str(format(drawerTotal, ".2f")) + '\nDrawer Initialized Amount: $' + str(self.drawerInitAmount))
            salesReportMessageBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Yes | QMessageBox.Cancel)
            salesReportMessageBox.setDefaultButton(QMessageBox.Ok)

            breakdownButton = salesReportMessageBox.button(QMessageBox.Yes)
            breakdownButton.setText('Sales Breakdown')

            salesReportMessageBox.exec_()

            if (salesReportMessageBox.clickedButton() == breakdownButton):
                self.saleReportPanel = SalesMenu()
                self.saleReportPanel.show()

        else:    
            # Spreadsheet Doesn't Exist
            QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)

    # Drawer Functions
    def drawerFunctions(self):

        # Get Admin Code
        text, ok = QInputDialog.getText(self, 'Admin Code', 'Enter Admin Code:', QLineEdit.Password)

        if ok:
            if (common.encode("bc6153", text) == db.get("code")):
                drawerMessageBox = QMessageBox()
                drawerMessageBox.setIcon(QMessageBox.Question)
                drawerMessageBox.setWindowTitle('Drawer Functions')
                drawerMessageBox.setText('Please select an action.')
                drawerMessageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Help | QMessageBox.Retry | QMessageBox.Cancel)
                drawerMessageBox.setDefaultButton(QMessageBox.Cancel)

                additionsButton = drawerMessageBox.button(QMessageBox.Yes)
                additionsButton.setText('Drawer Addition')

                deductionsButton = drawerMessageBox.button(QMessageBox.No)
                deductionsButton.setText('Drawer Deduction')

                initDrawerButton = drawerMessageBox.button(QMessageBox.Help)
                initDrawerButton.setText('Initialize Drawer')

                deinitDrawerButton = drawerMessageBox.button(QMessageBox.Retry)
                deinitDrawerButton.setText('Deinitialize Drawer')

                drawerMessageBox.exec_()

                if (drawerMessageBox.clickedButton() == additionsButton):
                    self.addToDrawer()
                elif (drawerMessageBox.clickedButton() == deductionsButton):
                    self.removeFromDrawer()
                elif (drawerMessageBox.clickedButton() == initDrawerButton):
                    self.initDrawer()
                elif (drawerMessageBox.clickedButton() == deinitDrawerButton):
                    self.deinitDrawer()
            else:

                # Invalid Code
                QMessageBox.warning(self, 'Invalid Code', "The admin code entered is not valid.", QMessageBox.Ok)

    # Initialize Drawer
    def initDrawer(self):

        if (self.drawerInitialized == True):
            QMessageBox.warning(self, 'Drawer Initialization', " Your drawer has already been initialized with $" + self.drawerInitAmount + ".", QMessageBox.Ok)
        else:
            amount, ok = QInputDialog.getText(self, 'Drawer Initialization', 'Enter amount to initialize your drawer:')
            if ok:

                # Check if Spreadsheet Exists
                if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
                    
                    # Openpyxl Initialization
                    workbook_name = db.get("sheet")
                    wb = load_workbook(workbook_name)
                    page = wb.active

                    # Append to Spreadsheet
                    page.append(["Drawer Initialization", "", float(amount), "", datetime.datetime.now(), ""])

                    # Save Spreadsheet
                    wb.save(db.get("sheet"))

                    # Init Drawer
                    self.drawerInitialized = True
                    self.drawerInitAmount = amount

                    QMessageBox.information(self, 'Drawer Initialization', " Your drawer has been initialized with $" + amount + ".", QMessageBox.Ok)
                else:    
                    # Spreadsheet Doesn't Exist
                    QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)

    # Deinitialize Drawer
    def deinitDrawer(self):

        if (self.drawerInitialized == True):

            # Check if Spreadsheet Exists
            if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
                
                # Openpyxl Initialization
                workbook_name = db.get("sheet")
                wb = load_workbook(workbook_name)
                page = wb.active

                # Append to Spreadsheet
                page.append(["Drawer Deinitialization", "", float("-" + self.drawerInitAmount), "", datetime.datetime.now(), ""])

                # Save Spreadsheet
                wb.save(db.get("sheet"))

                # Deinit Drawer
                self.drawerInitialized = False
                self.drawerInitAmount = 0

                QMessageBox.information(self, 'Drawer Deinitialization', " Your drawer has been deinitialized.", QMessageBox.Ok)
            else:    
                # Spreadsheet Doesn't Exist
                QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)
        else:
            QMessageBox.warning(self, 'Drawer Deinitialization', " Your drawer has already been deinitialized.", QMessageBox.Ok)

    # Add Cash to Drawer
    def addToDrawer(self):

        if (self.drawerInitialized == True):
            amount, ok = QInputDialog.getText(self, 'Drawer Addition', 'Enter amount to add to your drawer:')
            if ok:
                reason, ok = QInputDialog.getText(self, 'Drawer Addition', 'Reason for drawer addition:')
                if ok:

                    # Check if Spreadsheet Exists
                    if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
                        
                        # Openpyxl Initialization
                        workbook_name = db.get("sheet")
                        wb = load_workbook(workbook_name)
                        page = wb.active

                        # Append to Spreadsheet
                        page.append(["Drawer Addition", reason, float(amount), "", datetime.datetime.now(), ""])

                        # Save Spreadsheet
                        wb.save(db.get("sheet"))

                        QMessageBox.information(self, 'Drawer Addition', "$" + amount + " has been added to your cash box total.", QMessageBox.Ok)
                    else:    
                        # Spreadsheet Doesn't Exist
                        QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)
        else:
            QMessageBox.warning(self, 'Drawer Addition', " Your drawer has not yet been initialized.", QMessageBox.Ok)

    # Remove Cash to Drawer
    def removeFromDrawer(self):

        if (self.drawerInitialized == True):
            amount, ok = QInputDialog.getText(self, 'Drawer Deduction', 'Enter amount to deduct from your drawer:')
            if ok:
                reason, ok = QInputDialog.getText(self, 'Drawer Deduction', 'Reason for drawer deduction:')
                if ok:

                    # Check if Spreadsheet Exists
                    if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
                        
                        # Openpyxl Initialization
                        workbook_name = db.get("sheet")
                        wb = load_workbook(workbook_name)
                        page = wb.active

                        # Append to Spreadsheet
                        page.append(["Drawer Deduction", reason, float("-" + amount), "", datetime.datetime.now(), ""])

                        # Save Spreadsheet
                        wb.save(db.get("sheet"))

                        QMessageBox.information(self, 'Drawer Deduction', "$" + amount + " has been deducted from your cash box total.", QMessageBox.Ok)
                    else:    
                        # Spreadsheet Doesn't Exist
                        QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)
        else:
            QMessageBox.warning(self, 'Drawer Addition', " Your drawer has not yet been initialized.", QMessageBox.Ok)

    # Safely Exit App
    def exitApp(self):

        # Exit Dialog
        exitApplication = QMessageBox.question(self, 'Close Blue Crew POS Register?', "Are you sure that you would like to close Blue Crew POS Register?", QMessageBox.Yes | QMessageBox.No)
        
        if (exitApplication == QMessageBox.Yes):

            # Disconnect From Socket
            if (self.isServerSet == True):
                self.serverSocket.send(b'--QUIT--')

            # Exit App
            QCoreApplication.quit()

# Settings Window
class SettingsPanel(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()
        self.layout = QVBoxLayout(self)

        # Set Window Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        # Set Window Properties
        self.setGeometry(0, 0, 550, 300)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Register Settings')

        # Set Window Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)
 
        # Initialize Tab Screen
        self.tabs = QTabWidget()
        self.serversTab = QWidget()
        self.menuTab = QWidget()
        self.exportDataTab = QWidget()
        self.windowTab = QWidget()
        self.adminCodeTab = QWidget()
        self.aboutTab = QWidget()
        self.tabs.resize(500, 300) 
 
        # Add Tabs
        self.tabs.addTab(self.serversTab, "Server")
        # self.tabs.addTab(self.menuTab, "Menu Items")
        self.tabs.addTab(self.windowTab, "Window")
        self.tabs.addTab(self.exportDataTab, "Export Data")
        self.tabs.addTab(self.adminCodeTab, "Admin Code")
        self.tabs.addTab(self.aboutTab, "About")

        # Server Tab
        self.serversTab.layout = QVBoxLayout(self)
        self.addressBox = QHBoxLayout(self)

        self.addressLabel = QLabel(self)
        self.addressLabel.setText("Server Address:")

        self.addressField = QLineEdit()
        self.addressField.setText(db.get("serverAddress"))

        self.connectButton = QPushButton("Save")
        self.connectButton.clicked.connect(self.connectToServer)

        self.serverLabel = QLabel(self)
        self.serverLabel.setText("This allows you to connect a server for handling and managing orders as they come. You will need to restart Blue Crew POS Register in order to connect.")
        self.serverLabel.setWordWrap(True)

        self.addressBox.addWidget(self.addressLabel)
        self.addressBox.addWidget(self.addressField)
        self.addressBox.addWidget(self.connectButton)

        self.serversTab.layout.addLayout(self.addressBox)
        self.serversTab.layout.addWidget(self.serverLabel)
        self.serversTab.layout.addStretch()

        self.serversTab.setLayout(self.serversTab.layout)

        # # Menu Items Tab
        # self.menuTab.layout = QVBoxLayout(self)

        # self.editMenuButton = QPushButton("Edit Menu Items")
        # self.editMenuButton.clicked.connect(self.editMenu)

        # self.menuLabel = QLabel(self)
        # self.menuLabel.setText("This allows you to edit, and add menu items available for sale in Blue Crew POS Register.")
        # self.menuLabel.setWordWrap(True)

        # self.menuTab.layout.addWidget(self.editMenuButton)
        # self.menuTab.layout.addWidget(self.menuLabel)
        # self.menuTab.layout.addStretch()

        # self.menuTab.setLayout(self.menuTab.layout)

        # Window Tab
        self.windowTab.layout = QVBoxLayout(self)

        self.fullscreenWindow = QCheckBox('Enable Fullscreen', self)

        self.fullscreenEnabled = db.get("fullscreen")

        if (self.fullscreenEnabled == "True" or self.fullscreenEnabled == None):
            self.fullscreenWindow.toggle()

        self.fullscreenWindow.stateChanged.connect(self.toggleFullscreen)

        self.fullscreenLabel = QLabel(self)
        self.fullscreenLabel.setText("This setting changes whether Blue Crew POS Register runs in a fullscreen mode or a windowed mode.")
        self.fullscreenLabel.setWordWrap(True)

        self.windowTab.layout.addWidget(self.fullscreenWindow)
        self.windowTab.layout.addWidget(self.fullscreenLabel)
        self.windowTab.layout.addStretch()

        self.windowTab.setLayout(self.windowTab.layout)

        # Export Data Tab
        self.exportDataTab.layout = QVBoxLayout(self)
        self.locationBox = QHBoxLayout(self)

        self.sheetLabel = QLabel(self)
        self.sheetLabel.setText("Sheet Location:")

        self.sheetField = QLineEdit()
        self.sheetField.setText(db.get("sheet"))
        self.sheetField.returnPressed.connect(self.saveLocation)

        self.selectLocationButton = QPushButton("Select Location")
        self.selectLocationButton.clicked.connect(self.selectLocation)

        self.locationLabel = QLabel(self)
        self.locationLabel.setText("This allows you to choose the location of your spreadsheet where all of the POS data will be stored. The file format is xlsx.")
        self.locationLabel.setWordWrap(True)

        self.locationBox.addWidget(self.sheetLabel)
        self.locationBox.addWidget(self.sheetField)
        self.locationBox.addWidget(self.selectLocationButton)

        self.exportDataTab.layout.addLayout(self.locationBox)
        self.exportDataTab.layout.addWidget(self.locationLabel)
        self.exportDataTab.layout.addStretch()

        self.exportDataTab.setLayout(self.exportDataTab.layout)

        # Admin Code Tab
        self.adminCodeTab.layout = QVBoxLayout(self)
        self.codeBox = QHBoxLayout(self)

        self.adminCodeLabel = QLabel(self)
        self.adminCodeLabel.setText("Admin Code:")

        self.adminCodeField = QLineEdit()
        self.adminCodeField.setText(common.decode("bc6153", db.get("code")))
        self.adminCodeField.setEchoMode(QLineEdit.Password)

        self.changeCodeBtn = QPushButton("Change")
        self.changeCodeBtn.clicked.connect(self.changeCode)

        self.adminLabel = QLabel(self)
        self.adminLabel.setText("The admin code is a code that allows a user to access the settings within Blue Crew POS. Losing the code may require reinstalling the software.")
        self.adminLabel.setWordWrap(True)

        self.codeBox.addWidget(self.adminCodeLabel)
        self.codeBox.addWidget(self.adminCodeField)
        self.codeBox.addWidget(self.changeCodeBtn)

        self.adminCodeTab.layout.addLayout(self.codeBox)
        self.adminCodeTab.layout.addWidget(self.adminLabel)
        self.adminCodeTab.layout.addStretch()

        self.adminCodeTab.setLayout(self.adminCodeTab.layout)

        # About Tab
        self.aboutTab.layout = QVBoxLayout(self)

        # About Text
        self.aboutLabel = QLabel(self)
        self.aboutLabel.setText("Blue Crew POS Register is a system for collecting and logging orders.")
        self.aboutLabel.setWordWrap(True)

        # Version Label
        self.versionLabel = QLabel(self)
        self.versionLabel.setText("Version: " + db.get("version"))
        self.versionLabel.setWordWrap(True)

        # Copyright Label
        self.copyrightLabel = QLabel(self)
        self.copyrightLabel.setOpenExternalLinks(True)
        self.copyrightLabel.setText("\xa9 2018 <a href='https://bluecrew6153.org'>Blue Crew Robotics</a>. This application is licensed under the MIT license and the source code is available <a href='https://gitlab.com/BlueCrewRobotics/Blue-Crew-POS'>here</a>. Written by <a href='https://matthewgallant.me'>Matthew Gallant</a>.")
        self.copyrightLabel.setWordWrap(True)

        # Add Widgets to Terminals Tab
        self.aboutTab.layout.addWidget(self.aboutLabel)
        self.aboutTab.layout.addWidget(self.versionLabel)
        self.aboutTab.layout.addWidget(self.copyrightLabel)
        self.aboutTab.layout.addStretch()

        # Set Terminals Tab Layout
        self.aboutTab.setLayout(self.aboutTab.layout)
 
        # Add tabs to widget        
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def toggleFullscreen(self, state):
        
        if state == Qt.Checked:
            db.set("fullscreen", "True")
            self.fullscreenEnabled = True
            QMessageBox.information(self, 'Fullscreen Enabled', "Please restart Blue Crew POS Register for your changes to take effect.", QMessageBox.Ok)
        else:
            db.set("fullscreen", "False")
            self.fullscreenEnabled = False
            QMessageBox.information(self, 'Fullscreen Disabled', "Please restart Blue Crew POS Register for your changes to take effect.", QMessageBox.Ok)

    def editMenu(self):
        self.menuWindow = MenuItemsPanel()
        self.menuWindow.show()

    def selectLocation(self):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self,"Select Spreadsheet", os.path.expanduser("~"),"Excel Spreadsheet Files (*.xlsx)", options = options)
        if fileName:
            db.set("sheet", fileName)
            self.sheetField.setText(fileName)
            QMessageBox.information(self, 'Spreadsheet Location Saved', "The spreadsheet location has been saved.", QMessageBox.Ok)

    def saveLocation(self):
        db.set("sheet", self.sheetField.text())
        QMessageBox.information(self, 'Spreadsheet Location Saved', "The spreadsheet location has been saved.", QMessageBox.Ok)

    def connectToServer(self):
        db.set("serverAddress", self.addressField.text())
        QMessageBox.information(self, 'Server IP Address Saved', "The servers's IP address has been saved. Please restart Blue Crew POS Register in order to connect to the new IP address.", QMessageBox.Ok)

    def changeCode(self):
        db.set("code", common.encode("bc6153", self.adminCodeField.text()))
        QMessageBox.information(self, 'Admin Code Changed', "The admin code has been successfully changed!", QMessageBox.Ok)

# Edit Menu Window
class MenuItemsPanel(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()

        self.layout = QVBoxLayout(self)

        # Set Window Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        # Set Window Properties
        self.setGeometry(0, 0, 700, 500)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Register Menu Items')

        # Set Window Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        # Create Table
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(6)
        self.tableWidget.move(0,0)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Size"))
        self.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Price"))
        self.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem("Keycode"))
        self.tableWidget.setHorizontalHeaderItem(4, QTableWidgetItem("Edit"))
        self.tableWidget.setHorizontalHeaderItem(5, QTableWidgetItem("Delete"))

        # Add Menu Item Button
        self.addButton = QPushButton('Add Item', self)
        self.addButton.clicked.connect(self.addItemToOrder)

        # Close Window Button
        self.closeButton = QPushButton('Close', self)
        self.closeButton.clicked.connect(self.closeWindow)

        # Set Layouts
        self.bottom_hbox = QHBoxLayout()
        self.bottom_hbox.addWidget(self.addButton)
        self.bottom_hbox.addStretch()
        self.bottom_hbox.addWidget(self.closeButton)

        self.layout.addWidget(self.tableWidget)
        self.layout.addLayout(self.bottom_hbox)

        # Get Menu Items for Table
        items = self.getMenuItems()

        # Load Table
        for item in items:
            self.addToTable(item)

    # Add Item to Table
    def addToTable(self, item):

        code, name, size, price = self.getItems(item)

        if (code != False):

            cost = "$" + str(format(price, ".2f"))

            rowPosition = self.tableWidget.rowCount()
            self.tableWidget.insertRow(rowPosition)

            self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(name))
            self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(size))
            self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(cost))
            self.tableWidget.setItem(rowPosition, 3, QTableWidgetItem(code))

            editBtn = QPushButton(self.tableWidget)
            editBtn.setText('Edit')
            editBtn.clicked.connect(partial(self.editItem, code))
            self.tableWidget.setCellWidget(rowPosition, 4, editBtn)

            deleteBtn = QPushButton(self.tableWidget)
            deleteBtn.setText('Delete')
            deleteBtn.clicked.connect(partial(self.deleteItem, code))
            self.tableWidget.setCellWidget(rowPosition, 5, deleteBtn)

    def getMenuItems(self):

        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json') as f:
            data = json.load(f)

        items = []

        for menuItem in data:
            items.append(menuItem)
        
        return items
 
    def getItems(self, item):

        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json') as f:
            data = json.load(f)

        foundItem = False

        for menuItem in data:

            if (menuItem == item):
                return item, data[menuItem]['name'], data[menuItem]['size'], data[menuItem]['price']
                foundItem = True
            
        if (foundItem == False):
            return False, False, False, False

    def addItemToOrder(self):
        name, nameOk = QInputDialog.getText(self, 'Edit Item', 'Item Name:')
        if nameOk:
            size, sizeOk = QInputDialog.getText(self, 'Edit Item', 'Item Size:')
            if sizeOk:
                price, priceOk = QInputDialog.getText(self, 'Edit Item', 'Item Price:')
                if priceOk:
                    keycode, keycodeOk = QInputDialog.getText(self, 'Edit Item', 'Item Keycode:')
                    if keycodeOk:
                        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'r') as f:
                            data = json.load(f)

                            data[keycode] = {}
                            data[keycode]['name'] = name
                            data[keycode]['size'] = size
                            data[keycode]['price'] = float(price)    

                        os.remove(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json')
                        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'w') as f:
                            json.dump(data, f, indent=4)

                        self.tableWidget.setRowCount(0)

                        items = self.getMenuItems()

                        for item in items:
                            self.addToTable(item)

    def editItem(self, item):

        code, name, size, price = self.getItems(item)

        name, nameOk = QInputDialog.getText(self, 'Edit Item', 'Item Name:', QLineEdit.Normal, name)
        if nameOk:
            size, sizeOk = QInputDialog.getText(self, 'Edit Item', 'Item Size:', QLineEdit.Normal, size)
            if sizeOk:
                price, priceOk = QInputDialog.getText(self, 'Edit Item', 'Item Price:', QLineEdit.Normal, str(price))
                if priceOk:
                    keycode, keycodeOk = QInputDialog.getText(self, 'Edit Item', 'Item Keycode:', QLineEdit.Normal, code)
                    if keycodeOk:

                        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'r') as f:
                            data = json.load(f)

                            if (keycode != code):
                                data[keycode] = data[code]
                                del data[code]

                            data[keycode]['name'] = name
                            data[keycode]['size'] = size
                            data[keycode]['price'] = float(price)

                        os.remove(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json')
                        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'w') as f:
                            json.dump(data, f, indent=4)

                        self.tableWidget.setRowCount(0)

                        items = self.getMenuItems()

                        for item in items:
                            self.addToTable(item)

    def deleteItem(self, item):

        deleteItem = QMessageBox.question(self, 'Delete Menu Item?', "Are you sure that you would like to delete this menu item?", QMessageBox.Yes | QMessageBox.No)

        if (deleteItem == QMessageBox.Yes):
            with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'r') as f:
                data = json.load(f)
                del data[item]

            os.remove(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json')
            with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json', 'w') as f:
                json.dump(data, f, indent=4)

            self.tableWidget.setRowCount(0)

            items = self.getMenuItems()

            for item in items:
                self.addToTable(item)   

    def closeWindow(self):
        self.hide()         

class HelpPanel(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()

        self.layout = QVBoxLayout(self)

        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        self.setGeometry(0, 0, 700, 500)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Register Help')

        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        self.helpLabelOne = QLabel(self)
        self.helpLabelOne.setText("To add an item to an order, click \"Add Item to Order\" and type in the item code for the desired item. A list of available items and their associated keycodes are listed below:")
        self.helpLabelOne.setWordWrap(True)

        self.helpLabelTwo = QLabel(self)
        self.helpLabelTwo.setText("To complete an order, click \"Complete Order\". Hand out appropriate change based on the total due and then click \"Yes\". If you need to go back before completing the order, click \"No\".")
        self.helpLabelTwo.setWordWrap(True)

        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(4)
        self.tableWidget.move(0,0)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Size"))
        self.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Price"))
        self.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem("Keycode"))

        self.layout.addWidget(self.helpLabelOne)
        self.layout.addWidget(self.tableWidget)
        self.layout.addWidget(self.helpLabelTwo)

        items = self.getMenuItems()

        for item in items:
            self.addToTable(item)

    def addToTable(self, item):

        code, name, size, price = self.getItems(item)

        if (code != False):

            cost = "$" + str(format(price, ".2f"))

            rowPosition = self.tableWidget.rowCount()
            self.tableWidget.insertRow(rowPosition)

            self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(name))
            self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(size))
            self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(cost))
            self.tableWidget.setItem(rowPosition, 3, QTableWidgetItem(code))

    def getMenuItems(self):

        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json') as f:
            data = json.load(f)

        items = []

        for menuItem in data:
            items.append(menuItem)
        
        return items
 
    def getItems(self, item):

        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json') as f:
            data = json.load(f)

        foundItem = False

        for menuItem in data:

            if (menuItem == item):
                return item, data[menuItem]['name'], data[menuItem]['size'], data[menuItem]['price']
                foundItem = True
            
        if (foundItem == False):
            return False, False, False, False


class SalesMenu(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()

        self.layout = QVBoxLayout(self)

        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        self.setGeometry(0, 0, 800, 600)
        common.centerWindow(self)
        self.setWindowTitle('Sales Breakdown Report')

        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(6)
        self.tableWidget.move(0,0)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Size"))
        self.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Price"))
        self.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem("Payment Type"))
        self.tableWidget.setHorizontalHeaderItem(4, QTableWidgetItem("Date"))
        self.tableWidget.setHorizontalHeaderItem(5, QTableWidgetItem("Name on Order"))

        self.layout.addWidget(self.tableWidget)

        self.generateTable()

    def addToTable(self, name, size, price, payment, date, order):

        cost = "$" + str(format(price, ".2f"))

        rowPosition = self.tableWidget.rowCount()
        self.tableWidget.insertRow(rowPosition)

        self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(name))
        self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(size))
        self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(cost))
        self.tableWidget.setItem(rowPosition, 3, QTableWidgetItem(payment))
        self.tableWidget.setItem(rowPosition, 4, QTableWidgetItem(str(date)))
        self.tableWidget.setItem(rowPosition, 5, QTableWidgetItem(order))

    def generateTable(self):

        # Check if Spreadsheet Exists
        if (len(db.get("sheet")) > 0 and db.get("sheet")[-4:] == "xlsx" and os.path.isfile(db.get("sheet"))):
            
            # Openpyxl Initialization
            workbook_name = db.get("sheet")
            wb = load_workbook(workbook_name)
            page = wb.active

            grossSales = 0
            drawerTotal = 0

            for row in page.iter_rows(min_row=2):
                self.addToTable(row[0].value, row[1].value, row[2].value, row[3].value, row[4].value, row[5].value)

        else:    
            # Spreadsheet Doesn't Exist
            QMessageBox.warning(self, 'Spreadsheet Error', "Please select a spreadsheet to use for data collection in the settings menu.", QMessageBox.Ok)
 
    def getItems(self, item):

        with open(self.scriptDir + os.path.sep + 'register' + os.path.sep + 'items.json') as f:
            data = json.load(f)

        foundItem = False

        for menuItem in data:

            if (menuItem == item):
                return item, data[menuItem]['name'], data[menuItem]['size'], data[menuItem]['price']
                foundItem = True
            
        if (foundItem == False):
            return False, False, False, False

if __name__ == '__main__':
    
    app = QApplication(['Blue Crew POS Register'])
    ex = POS()

    # app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    # First Time Setup
    if (db.get("code") == None):
        text, ok = QInputDialog.getText(ex, 'Admin Code', 'Please choose an admin code for Blue Crew POS Register:', QLineEdit.Password)
        if ok and len(text) > 0:
            ex.setupAdminCode(text)

    sys.exit(app.exec_())
