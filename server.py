#
# Blue Crew Point of Sale Server
# (c) 2018 Blue Crew Robotics
# Author: Matt Gallant
#

import socket
import sys
import os
import json
import traceback
import uuid
import platform

from threading import Thread

class Server():

    def __init__(self):

        self.connections = []
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))

        self.start_server()

    def start_server(self):
        host = socket.gethostbyname('0.0.0.0')
        port = 8888         # arbitrary non-privileged port

        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)   # SO_REUSEADDR flag tells the kernel to reuse a local socket in TIME_WAIT state, without waiting for its natural timeout to expire
        print("Socket created")

        try:
            soc.bind((host, port))
        except:
            print("Bind failed. Error : " + str(sys.exc_info()))
            sys.exit()

        soc.listen(5)       # queue up to 5 requests
        print("Socket now listening")

        # Set IP Address Label to IP Address of Computer
        if (platform.system() == "Linux"):
            print("IP Address: " + [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])
        else:
            print("IP Address: " + socket.gethostbyname(socket.gethostname()))

        # infinite loop- do not reset for every requests
        while True:
            connection, address = soc.accept()
            ip, port = str(address[0]), str(address[1])
            print("Connected with " + ip + ":" + port)

            try:
                Thread(target = self.client_thread, args = (connection, ip, port)).start()
                self.connections.append(connection)
            except:
                print("Thread did not start.")
                traceback.print_exc()

        soc.close()


    def client_thread(self, connection, ip, port, max_buffer_size = 5120):
        is_active = True

        while is_active:
            client_input = self.receive_input(connection, max_buffer_size)

            if (client_input != False):
                if "--QUIT--" in client_input:
                    print("Client is requesting to quit")
                    connection.close()
                    print("Connection " + ip + ":" + port + " closed")
                    is_active = False
                    self.connections.remove(connection)
                else:
                    print("Processed result: {}".format(client_input))
                    print(client_input)
                    self.broadcast(client_input)
            else:
                connection.sendall("-".encode("utf8"))

    def broadcast(self, message):
        for conn in self.connections:
            conn.sendall(message.encode("utf8"))

    def receive_input(self, connection, max_buffer_size):

        client_input = connection.recv(max_buffer_size)
        client_input_size = sys.getsizeof(client_input)

        if client_input_size > max_buffer_size:
            print("The input size is greater than expected {}".format(client_input_size))

        decoded_input = client_input.decode("utf8").rstrip()  # decode and strip end of line
        result = self.process_input(decoded_input)

        if (result != False):
            return result
        else:
            return False

    def getItems(self, item):

        with open(self.scriptDir + os.path.sep + "register" + os.path.sep + 'items.json') as f:
            data = json.load(f)

        foundItem = False

        for menuItem in data:

            if (menuItem == item):
                return item, data[menuItem]['name'], data[menuItem]['size'], data[menuItem]['price']
                foundItem = True
            
        if (foundItem == False):
            return False, False, False, False


    def process_input(self, input_str):
        print("Processing the input received from client")

        print(input_str)

        order = {}

        if (input_str == "--QUIT--"):
            return input_str
        elif (input_str[0:1] == "t"):
            print(input_str)
            return input_str
        elif (input_str[0:1] == "q"):
            
            totalData = input_str[2:]

            if (totalData == "test"):
                code, name, size, price = "test", "Test Item", "Test Size", "0.00"
            else:
                code, name, size, price = self.getItems(totalData)
            
            if (code != False):
                print("no error")
                return "s/" + code + "::" + name + "::" + size + "::" + str(price)
            else:
                print("error")
                return False
        else:
            orderName, orderData = input_str[2:].split("|")
            totalData = orderData.split(", ")

            print("TOTAL DATA: " + str(totalData))

            order["name"] = orderName
            order["order"] = {}

            itemNumber = 0
            error = False

            for item in totalData:
                
                if (item == "test"):
                    code, name, size, price = "test", "Test Item", "Test Size", "0.00"
                else:
                    code, name, size, price = self.getItems(item)

                if (code != False):

                    order["order"][itemNumber] = {}

                    order["order"][itemNumber]["item"] = name
                    order["order"][itemNumber]["size"] = size
                    order["order"][itemNumber]["id"] = str(uuid.uuid4())

                    itemNumber += 1
                else:
                    error = True
            
            if (error != True):
                print("no error")
                return "r/" + json.dumps(order)
            else:
                print("error")
                return False

if __name__ == "__main__":
    Server()
