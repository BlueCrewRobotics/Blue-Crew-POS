#
# Blue Crew Point of Sale Terminal
# (c) 2018 Blue Crew Robotics
# Author: Matt Gallant
#

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from openpyxl import *
from functools import partial
from socket import socket, gethostbyname, gethostname, getfqdn, AF_INET, SOCK_STREAM
from playsound import playsound
from threading import Thread

import datetime
import sys
import os
import ctypes
import time
import platform
import json

import libraries.pydb as db
import libraries.common as common

# Main GUI Thread
class POS(QWidget):
    
    def __init__(self):
        super().__init__()

        # Check if Server is Enabled
        self.serverEnabled = db.get("server")

        # Sets App ID on Windows Necessary to Show a Taskbar Icon
        if (platform.system() == "Windows"):
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('org.bluecrew6153.terminal')

        # Initialize Variables
        self.order = []
        self.itemNumber = 0
        self.colorPosition = 0
        self.colorDict = {}
        self.serverConnected = False
        
        # Set Available Colors for Rows
        self.colors = {
            0: QColor(255, 152, 0),
            1: QColor(76, 175, 80),
            2: QColor(244, 67, 54),
            3: QColor(156, 39, 176)
        }

        # Initialize GUI
        self.initUI()

    # Collect and Process Order
    def collectOrder(self, order):

        # Get Order Data
        orderData = json.loads(order[2:])

        # Retrieve Color
        color = self.colors.get(self.colorPosition, "Invalid Color")

        # Cycle Color Loop
        if (self.colorPosition == 3):
            self.colorPosition = 0
        else:
            self.colorPosition += 1

        # Send Data to Appropriate Places
        for data in orderData["order"]:

            sendData = orderData["order"][data]["item"] + "::" + orderData["order"][data]["size"] + "::" + orderData["name"] + "::" + orderData["order"][data]["id"]

            self.colorDict[orderData["order"][data]["id"]] = color
            self.order.append(sendData)
            self.addToTable(sendData)
        
    # Runs on a Keypress
    def keyPressEvent(self, event):

        # Complete Item on Return Key Press
        if event.key() == Qt.Key_Return:
            rows = sorted(set(index.row() for index in self.tableWidget.selectedIndexes()))
            for row in rows:
                self.completeItem(row)

        # Finish Event
        event.accept()

    # Runs when Clicking the "X" Button in the Corner
    def closeEvent(self, event):

        # Safely Exit App
        self.exitApp()

        # Proceed
        event.ignore()
        
    # Initializes the Main GUI
    def initUI(self):

        # Set App Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png'))

        # Create Logo in Main Window
        self.logo = QLabel(self)
        logoImage = QPixmap(self.scriptDir + os.path.sep + 'icons' + os.path.sep + 'icon.png').scaled(64, 64)
        self.logo.setPixmap(logoImage)

        # Create App Title in Main Window
        self.titleLabel = QLabel(self)
        self.titleLabel.setText("Blue Crew POS Terminal")
        self.titleLabel.setFont(QFont("Helvetica", 20, QFont.Bold))

        # Create Settings Button in Main Window
        self.settingsButton = QPushButton('Settings', self)
        self.settingsButton.clicked.connect(self.openSettings)

        # Create Help Button in Main Window
        self.helpButton = QPushButton('Help', self)
        self.helpButton.clicked.connect(self.help)

        # Create Exit Button in Main Window
        self.exitButton = QPushButton('Exit', self)
        self.exitButton.clicked.connect(self.exitApp)

        # Check if Fullscreen Mode is Enabled
        self.fullscreenEnabled = db.get("fullscreen")

        # Set Window Details
        self.setGeometry(0, 0, 800, 500)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Terminal')

        # Enable Fullscreen Mode if Needed
        if (self.fullscreenEnabled == "True" or self.fullscreenEnabled == None):
            self.showFullScreen()

        # Set Window Backgrounf Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        # Create Main Table
        self.createTable()

        # Create Top HBox
        self.top_hbox = QHBoxLayout()
        self.top_hbox.addWidget(self.logo)
        self.top_hbox.addStretch()
        self.top_hbox.addWidget(self.titleLabel)

        # Create Bottom HBox
        self.bottom_hbox = QHBoxLayout()
        self.bottom_hbox.addWidget(self.helpButton)
        self.bottom_hbox.addStretch()
        self.bottom_hbox.addWidget(self.settingsButton)
        self.bottom_hbox.addStretch()
        self.bottom_hbox.addWidget(self.exitButton)
 
        # Create VBox for Entire Window
        self.layout = QVBoxLayout()
        self.layout.addLayout(self.top_hbox)
        self.layout.addSpacing(10)
        self.layout.addWidget(self.tableWidget)
        self.layout.addLayout(self.bottom_hbox)
        self.setLayout(self.layout)
 
        # Show Window
        self.show()

        # Enable Server Thread if Needed
        if (self.serverEnabled == "True"):

            # Receive Data Thread
            try:
                Thread(target = self.receiveThread, daemon = True).start()
            except:
                print("Thread did not start.")
                traceback.print_exc()

            self.serverConnected = True

        # Else if First Start, Turn it on
        elif (self.serverEnabled == None):

            # Receive Data Thread
            try:
                Thread(target = self.receiveThread, daemon = True).start()
            except:
                print("Thread did not start.")
                traceback.print_exc()

            db.set("server", "True")

        # Else, Show Error
        else:
            QMessageBox.information(self, 'Server Disabled', "Please enable the server to use Blue Crew POS Terminal.", QMessageBox.Ok)

    def receiveThread(self):

        # Get Current Directory of Script
        scriptDir = os.path.dirname(os.path.realpath(__file__))

        # Create Socket
        self.soc = socket(AF_INET, SOCK_STREAM)

        # Socket Variables
        self.host = db.get("serverAddress")
        self.port = 8888

        # Try to Connect to Socket
        try:
            self.soc.connect((self.host, self.port))
            self.serverConnected = True
        except:
            print("Connection error")
            # QMessageBox.information(self, 'Server Disabled', "Please enable the server to use Blue Crew POS Terminal.", QMessageBox.Ok)
            self.serverConnected = False

        while True:

            data = self.soc.recv(5120).decode("utf8")
            
            try:
                ignore, data = data.split("r/")
                data = "r/" + data
            except:
                pass

            print("JUST IN: " + data)

            if (data[0:1] == 'r'):

                # Send Data back to GUI Thread
                self.collectOrder(str(data))

                # Play Bell Sound
                playsound(scriptDir + os.path.sep + 'terminal' + os.path.sep + 'bell.mp3')
            
            elif (data[0:1] == "t"):

                # Remove Order as per Servers Request
                self.completeOrderByUUID(data[2:])

    def completeOrderByUUID(self, uuid):

        # Init Variables
        uuidCount = -1
        uuids = []

        for order in self.order:

            # Increment UUID Count
            uuidCount += 1

            # Get UUID
            itemName, size, name, uuidQuery = order.split("::")

            # Append to Array
            uuids.append(uuidQuery)

            # Find UUID Index
            if (uuidQuery == uuid):
                break

        if (uuid in uuids):

            # Remove Item from List
            self.order.pop(uuidCount)

            # Remove Color Entry
            del self.colorDict[uuid]

            # Reset Item Number
            self.itemNumber = 0

            # Erase Table
            self.tableWidget.setRowCount(0)

            # Reload Table
            for name in self.order:
                self.addToTable(name)

            # Select First Item in Table
            self.tableWidget.setCurrentCell(0, 0)

    # Show Help Window
    def help(self):
        self.helpWindow = HelpPanel()
        self.helpWindow.show()

    # Set Admin Code
    def setupAdminCode(self, code):
        db.set("code", common.encode("bc6153", code))

    # Create Main Order Table
    def createTable(self):

        # Create Table
        self.tableWidget = QTableWidget()

        # Set Row Count to Zero to Start
        self.tableWidget.setRowCount(0)

        # Set Column Count to Three (Name, Size, Complete)
        self.tableWidget.setColumnCount(3)

        # Move Table Position
        self.tableWidget.move(0,0)

        # Evenly Stretch Table Headers
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # Make Table Uneditable
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)

        # Set Table Headers
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Item"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Size"))
        self.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Name"))

    # Add Item to Table
    def addToTable(self, item):

        # Split Item Text
        item, size, name, uuid = item.split("::")

        # Get Row Color
        color = self.colorDict[uuid]

        # Append Row to Table
        rowPosition = self.tableWidget.rowCount()
        self.tableWidget.insertRow(rowPosition)

        # Set Text in Row
        self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(item))
        self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem(size))
        self.tableWidget.setItem(rowPosition, 2, QTableWidgetItem(name))

        # Color Row
        self.setColortoRow(rowPosition, color)

        # Add to Item Number
        self.itemNumber += 1

        # Cursor Placement
        if (rowPosition == 0):
            # Select First Item in Table
            self.tableWidget.setCurrentCell(0, 0)

    # Sets the Color of the Rows of an Order
    def setColortoRow(self, rowIndex, color):
        for j in range(self.tableWidget.columnCount()):
            self.tableWidget.item(rowIndex, j).setBackground(color)

    # Complete Item/Remove Item from Table
    def completeItem(self, item):

        # Ask to Complete Item
        completeItem = QMessageBox.question(self, 'Complete Item?', "Are you sure that you would like to mark the item as complete?", QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        
        # If yes, Remove Item.
        if (completeItem == QMessageBox.Yes):
            if (len(self.order) > 0):

                # Get UUID
                itemName, size, name, uuid = self.order[item].split("::")

                # Remove From Server
                self.soc.sendall(str('t/' + uuid).encode("utf8"))

                # Remove Item from List
                self.order.pop(item)

                # Reset Item Number
                self.itemNumber = 0

                # Erase Table
                self.tableWidget.setRowCount(0)

                # Reload Table
                for name in self.order:
                    self.addToTable(name)

                # Select First Item in Table
                self.tableWidget.setCurrentCell(0, 0)

    # Open Settings Window
    def openSettings(self):

        # Ask for Admin Code
        text, ok = QInputDialog.getText(self, 'Admin Code', 'Enter Admin Code:', QLineEdit.Password)

        if ok:

            # Try to Open Window
            if (common.encode("bc6153", text) == db.get("code")):

                # Open Window
                self.settingsWindow = SettingsPanel()
                self.settingsWindow.show()
            else:

                # Display Error
                QMessageBox.warning(self, 'Incorrect Code!', "The admin code entered is not valid!", QMessageBox.Ok)

    # Safely Exit Application
    def exitApp(self):

        # Ask to Exit
        exitApplication = QMessageBox.question(self, 'Close Blue Crew POS Terminal?', "Are you sure that you would like to close Blue Crew POS Terminal?", QMessageBox.Yes | QMessageBox.No)
        
        if (exitApplication == QMessageBox.Yes):

            # Disconnect from Socket
            if (self.serverConnected == True):
                self.soc.sendall('--QUIT--'.encode("utf8"))
            
            # Exit Application
            QCoreApplication.quit()

# Settings Window
class SettingsPanel(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()

        # Set Layout
        self.layout = QVBoxLayout(self)

        # Check is Server is Enabled
        self.serverEnabled = db.get("server")

        # Set Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icon.png'))

        # Set Window Settings
        self.setGeometry(0, 0, 500, 300)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Terminal Settings')

        # Set Window Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)
 
        # Initialize Tab Screen
        self.tabs = QTabWidget()
        self.serversTab = QWidget()
        self.windowTab = QWidget()
        self.adminCodeTab = QWidget()
        self.aboutTab = QWidget()
        self.tabs.resize(500, 300) 
 
        # Add Tabs
        self.tabs.addTab(self.serversTab, "Server")
        self.tabs.addTab(self.windowTab, "Window")
        self.tabs.addTab(self.adminCodeTab, "Admin Code")
        self.tabs.addTab(self.aboutTab, "About")

        # Server Tab
        self.serversTab.layout = QVBoxLayout(self)
        self.addressBox = QHBoxLayout(self)

        self.addressLabel = QLabel(self)
        self.addressLabel.setText("Server Address:")

        self.addressField = QLineEdit()
        self.addressField.setText(db.get("serverAddress"))

        self.connectButton = QPushButton("Save")
        self.connectButton.clicked.connect(self.connectToServer)

        self.serverLabel = QLabel(self)
        self.serverLabel.setText("This allows you to connect a server for handling and managing orders as they come. You will need to restart Blue Crew POS Register in order to connect.")
        self.serverLabel.setWordWrap(True)

        self.addressBox.addWidget(self.addressLabel)
        self.addressBox.addWidget(self.addressField)
        self.addressBox.addWidget(self.connectButton)

        self.serversTab.layout.addLayout(self.addressBox)
        self.serversTab.layout.addWidget(self.serverLabel)
        self.serversTab.layout.addStretch()

        self.serversTab.setLayout(self.serversTab.layout)

        # Window Tab
        self.windowTab.layout = QVBoxLayout(self)

        # Fullscreen Checkbox
        self.fullscreenWindow = QCheckBox('Enable Fullscreen', self)

        # Check if Fullscreen is Enabled
        self.fullscreenEnabled = db.get("fullscreen")

        # Toggle Checkbox if Fullscreen is Enabled
        if (self.fullscreenEnabled == "True" or self.fullscreenEnabled == None):
            self.fullscreenWindow.toggle()

        # Run Toggle Fullscreen on Toggle Change
        self.fullscreenWindow.stateChanged.connect(self.toggleFullscreen)

        # Fullscreen Information Label
        self.fullscreenLabel = QLabel(self)
        self.fullscreenLabel.setText("This setting changes whether Blue Crew POS Terminal runs in a fullscreen mode or a windowed mode.")
        self.fullscreenLabel.setWordWrap(True)

        # Add Widgets to Windows Tab
        self.windowTab.layout.addWidget(self.fullscreenWindow)
        self.windowTab.layout.addWidget(self.fullscreenLabel)
        self.windowTab.layout.addStretch()

        # Set Windows Tab Layout
        self.windowTab.setLayout(self.windowTab.layout)

        # Admin Code Tab
        self.adminCodeTab.layout = QVBoxLayout(self)
        self.codeBox = QHBoxLayout(self)

        # Code Text Label
        self.adminCodeLabel = QLabel(self)
        self.adminCodeLabel.setText("Admin Code:")

        # Code Text Field
        self.adminCodeField = QLineEdit()
        self.adminCodeField.setText(common.decode("bc6153", db.get("code")))
        self.adminCodeField.setEchoMode(QLineEdit.Password)

        # Code Change Button
        self.changeCodeBtn = QPushButton("Change")
        self.changeCodeBtn.clicked.connect(self.changeCode)

        # Code Information Label
        self.adminLabel = QLabel(self)
        self.adminLabel.setText("The admin code is a code that allows a user to access the settings within Blue Crew POS. Losing the code may require reinstalling the software.")
        self.adminLabel.setWordWrap(True)

        # Add Widgets to HBox
        self.codeBox.addWidget(self.adminCodeLabel)
        self.codeBox.addWidget(self.adminCodeField)
        self.codeBox.addWidget(self.changeCodeBtn)

        # Add Items to VBox
        self.adminCodeTab.layout.addLayout(self.codeBox)
        self.adminCodeTab.layout.addWidget(self.adminLabel)
        self.adminCodeTab.layout.addStretch()

        # Set Layout of Admin Code Tab
        self.adminCodeTab.setLayout(self.adminCodeTab.layout)

        # About Tab
        self.aboutTab.layout = QVBoxLayout(self)

        # About Text
        self.aboutLabel = QLabel(self)
        self.aboutLabel.setText("Blue Crew POS Terminal is a system for collecting and completing orders placed on a computer running the Blue Crew POS Register software.")
        self.aboutLabel.setWordWrap(True)

        # Version Label
        self.versionLabel = QLabel(self)
        self.versionLabel.setText("Version: " + db.get("version"))
        self.versionLabel.setWordWrap(True)

        # Copyright Label
        self.copyrightLabel = QLabel(self)
        self.copyrightLabel.setOpenExternalLinks(True)
        self.copyrightLabel.setText("\xa9 2018 <a href='https://bluecrew6153.org'>Blue Crew Robotics</a>. This application is licensed under the MIT license and the source code is available <a href='https://gitlab.com/BlueCrewRobotics/Blue-Crew-POS'>here</a>. Written by <a href='https://matthewgallant.me'>Matthew Gallant</a>.")
        self.copyrightLabel.setWordWrap(True)

        # Add Widgets to Terminals Tab
        self.aboutTab.layout.addWidget(self.aboutLabel)
        self.aboutTab.layout.addWidget(self.versionLabel)
        self.aboutTab.layout.addWidget(self.copyrightLabel)
        self.aboutTab.layout.addStretch()

        # Set Terminals Tab Layout
        self.aboutTab.setLayout(self.aboutTab.layout)
 
        # Add Tabs to Widget        
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    # Toggle Fullscreen
    def toggleFullscreen(self, state):
        
        if state == Qt.Checked:

            # Set Database Variable
            db.set("fullscreen", "True")

            # Set Local Variable
            self.fullscreenEnabled = True

            # Tell User to Restart POS
            QMessageBox.information(self, 'Fullscreen Enabled', "Please restart Blue Crew POS Terminal for your changes to take effect.", QMessageBox.Ok)
        else:

            # Set Database Variable
            db.set("fullscreen", "False")

            # Set Local Variable
            self.fullscreenEnabled = False

            # Tell User to Restart POS
            QMessageBox.information(self, 'Fullscreen Disabled', "Please restart Blue Crew POS Terminal for your changes to take effect.", QMessageBox.Ok)

    # Save Server IP Address
    def connectToServer(self):
        db.set("serverAddress", self.addressField.text())
        QMessageBox.information(self, 'Server IP Address Saved', "The server's IP address has been saved. Please restart Blue Crew POS Terminal in order to connect to the new IP address.", QMessageBox.Ok)

    # Change Admin Code
    def changeCode(self):

        # Set Database Variable
        db.set("code", common.encode("bc6153", self.adminCodeField.text()))

        # Success Dialog
        QMessageBox.information(self, 'Admin Code Changed', "The admin code has been successfully changed!", QMessageBox.Ok)

# Help Window
class HelpPanel(QWidget):        
 
    def __init__(self):   
        super(QWidget, self).__init__()

        # Set Layout
        self.layout = QVBoxLayout(self)

        # Set Icon
        self.scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QIcon(self.scriptDir + os.path.sep + 'icon.png'))

        # Set Window Settings
        self.setGeometry(0, 0, 400, 100)
        common.centerWindow(self)
        self.setWindowTitle('Blue Crew POS Terminal Help')

        # Set Window Color
        palette = self.palette()
        palette.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(palette)

        # Help Label
        self.helpLabel = QLabel(self)
        self.helpLabel.setText("To mark an item as complete, either click the \"Complete\" button next to the item, or if the item is the first item in the list, you can press the Enter/Return key to complete it.")
        self.helpLabel.setWordWrap(True)

        # Add Widgets to Window
        self.layout.addWidget(self.helpLabel)

# Run Main Function
if __name__ == '__main__':
    
    # Set Application Name and Start it
    app = QApplication(['Blue Crew POS Terminal'])
    ex = POS()

    # app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    # If no Code Exists, Set One
    if (db.get("code") == None):

        # Password Dialog
        text, ok = QInputDialog.getText(ex, 'Admin Code', 'Please choose an admin code for Blue Crew POS Terminal:', QLineEdit.Password)
        if ok and len(text) > 0:

            # Set Admin Code
            ex.setupAdminCode(text)

    sys.exit(app.exec_())
