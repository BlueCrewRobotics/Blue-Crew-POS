#
# Common Functions for Blue Crew POS
# (c) 2018 Blue Crew Robotics
# Author: Matt Gallant
#

from PyQt5.QtWidgets import *

import base64

def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc).encode()).decode()

def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc).decode()
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)

def centerWindow(self):

    # Get Frame Geometry
    qr = self.frameGeometry()

    # Get Center of Frame
    cp = QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)

    # Move Window
    self.move(qr.topLeft())