#!/bin/bash

echo "Blue Crew POS Register Installer for Ubuntu 18.04"

echo "Installing Python 3/Pip 3..."
sudo apt install python3 python3-pip

echo "Installing Python 3 Libraries..."
sudo pip3 install openpyxl pyqt5

echo "Starting Installation..."
BASEDIR=$(dirname "$0")
cd $BASEDIR

echo "Creating Directories..."
mkdir ~/.blue-crew-pos-register/
mkdir ~/.blue-crew-pos-register/icons/
mkdir ~/.blue-crew-pos-register/register/
mkdir ~/.blue-crew-pos-register/libraries/
mkdir ~/.blue-crew-pos-register/.pydb/

echo "Copying Files..."
cp -a ./run-register.sh ~/.blue-crew-pos-register/run-register.sh
cp -a ../register.py ~/.blue-crew-pos-register/register.py
cp -a ../.pydb/version.data ~/.blue-crew-pos-register/.pydb/version.data
cp -a ../icons/. ~/.blue-crew-pos-register/icons/
cp -a ../register/items.json ~/.blue-crew-pos-register/register/items.json
cp -a ../libraries/. ~/.blue-crew-pos-register/libraries/

echo "Creating Desktop Shortcut..."
cp -a ./blue-crew-pos-register.desktop ~/.local/share/applications/blue-crew-pos-register.desktop

cd ~/.blue-crew-pos-register/
INSTALLDIR=$(pwd)

echo "Icon=$INSTALLDIR/icons/icon.png" >> ~/.local/share/applications/blue-crew-pos-register.desktop

chmod +x ~/.local/share/applications/blue-crew-pos-register.desktop
chmod +x ~/.blue-crew-pos-register/run-register.sh

echo "Finished Installing Blue Crew POS Register!"

