#!/bin/bash

echo "Blue Crew POS Register Uninstaller for Ubuntu 18.04"

echo "Removing Application Files..."
rm -Rf ~/.blue-crew-pos-register/

echo "Removing Desktop Shortcut..."
rm ~/.local/share/applications/blue-crew-pos-register.desktop

echo "Finished Uninstalling Blue Crew POS Register!"

