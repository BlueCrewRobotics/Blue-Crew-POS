#!/bin/bash

echo "Blue Crew POS Terminal Uninstaller for Ubuntu 18.04"

echo "Removing Application Files..."
rm -Rf ~/.blue-crew-pos-terminal/

echo "Removing Desktop Shortcut..."
rm ~/.local/share/applications/blue-crew-pos-terminal.desktop

echo "Finished Uninstalling Blue Crew POS Terminal!"

