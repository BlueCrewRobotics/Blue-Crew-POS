#!/bin/bash

echo "Blue Crew POS Terminal Installer for Ubuntu 18.04"

echo "Installing Python 3/Pip 3..."
sudo apt install python3 python3-pip

echo "Installing Python 3 Libraries..."
sudo pip3 install playsound pyqt5

echo "Starting Installation..."
BASEDIR=$(dirname "$0")
cd $BASEDIR

echo "Creating Directories..."
mkdir ~/.blue-crew-pos-terminal/
mkdir ~/.blue-crew-pos-terminal/icons/
mkdir ~/.blue-crew-pos-terminal/terminal/
mkdir ~/.blue-crew-pos-terminal/libraries/
mkdir ~/.blue-crew-pos-terminal/.pydb/

echo "Copying Files..."
cp -a ./run-terminal.sh ~/.blue-crew-pos-terminal/run-terminal.sh
cp -a ../terminal.py ~/.blue-crew-pos-terminal/terminal.py
cp -a ../.pydb/version.data ~/.blue-crew-pos-terminal/.pydb
cp -a ../icons/. ~/.blue-crew-pos-terminal/icons/
cp -a ../terminal/. ~/.blue-crew-pos-terminal/terminal/
cp -a ../libraries/. ~/.blue-crew-pos-terminal/libraries/

echo "Creating Desktop Shortcut..."
cp -a ./blue-crew-pos-terminal.desktop ~/.local/share/applications/blue-crew-pos-terminal.desktop

cd ~/.blue-crew-pos-terminal/
INSTALLDIR=$(pwd)

echo "Icon=$INSTALLDIR/icons/icon.png" >> ~/.local/share/applications/blue-crew-pos-terminal.desktop

chmod +x ~/.local/share/applications/blue-crew-pos-terminal.desktop
chmod +x ~/.blue-crew-pos-terminal/run-terminal.sh

echo "Finished Installing Blue Crew POS Terminal!"

