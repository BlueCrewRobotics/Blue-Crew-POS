rd /s /q "..\binaries\Windows\Register\"

pyinstaller ^
    --hidden-import PyQt5.sip ^
    --noconsole -i ..\icons\icon.ico ^
    --add-data="..\icons\icon.png;icons" ^
    --add-data="..\register\items.json;register" ^
    --add-data="..\icons\add.png;icons" ^
    --add-data="..\icons\cart.png;icons" ^
    --add-data="..\icons\clear.png;icons" ^
    --add-data="..\.pydb\version.data;.pydb" ^
    --distpath ..\binaries\Windows\ ^
    -n Register ..\register.py

rd /s /q ".\build\"
del ".\Register.spec"