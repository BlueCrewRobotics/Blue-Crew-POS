rd /s /q "..\binaries\Windows\Terminal\"

pyinstaller ^
    --hidden-import PyQt5.sip ^
    --noconsole -i ..\icons\icon.ico ^
    --add-data="..\icons\icon.png;icons" ^
    --add-data="..\terminal\bell.mp3;terminal" ^
    --add-data="..\.pydb\version.data;.pydb" ^
    --distpath ..\binaries\Windows\ ^
    -n Terminal ..\terminal.py

rd /s /q ".\build\"
del ".\Terminal.spec"